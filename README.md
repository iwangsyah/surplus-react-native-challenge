# Surplus React Native Challenge - Wahyu

![](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExMDA5MzRlNDllN2UxYTYwMzEyNDMzODFjYWJjYTcyMGEwMjBhOTQ1MCZjdD1n/tPZSZBgjUdGMaSFjZN/giphy.gif)

### Run project in development

1. Clone the repo
   ```sh
   git clone https://gitlab.com/iwangsyah/surplus-react-native-challenge.git
   ```
2. Install dependencies
   ```sh
   npm install
   ```
   or
   
   ```sh
   yarn install
   ```
3. Install Podfile for iOS
   ```sh
   npx pod-install
   ```
    or
  
   ```sh
   npx pod install
   ```
4. Start the app
  - Run on Android: `npx react-native run-android` (or `yarn android`)  (or `npm run android`).

  - Run on iOS: `npx react-native run-ios` (or `yarn ios`) (or `npm run ios`).


# Generating the APK
Run the following in a terminal:

```
$ cd android
$ ./gradlew assembleRelease
```

The generated APK can be found under:
```
android/app/build/outputs/apk/release/app-release.apk
```
