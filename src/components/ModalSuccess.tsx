import React from 'react';
import {StyleSheet, Image, View, Text, Dimensions} from 'react-native';
import Modal from 'react-native-modal';
import Images from '../assets/images';
import {TextStyles} from '../styles';

interface ModalProps {
  title: string;
  visible: boolean;
}

const ModalSuccess: React.FC<ModalProps> = ({
   title, 
   visible, 
}) => {
  return (
    <Modal animationIn="slideInUp" isVisible={visible}>
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <View style={styles.modalView}>
          <Image
            source={Images.icSuccess}
            style={styles.icon}
            resizeMode="contain"
          />
          <Text style={TextStyles.Caption14Bold}>
            {title}
          </Text>
        </View>
      </View>
    </Modal>
  );
}

export default ModalSuccess

const styles = StyleSheet.create({
  modalView: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    borderRadius: 12,
    padding: 24,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  icon: {
    width: 32, 
    height: 32, 
    marginRight: 16
  }
});
