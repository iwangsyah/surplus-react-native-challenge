import React from 'react';
import { View, Image, TouchableOpacity, StyleSheet, Text, StatusBar, SafeAreaView, StatusBarProps, Platform} from 'react-native';
import { useIsFocused } from '@react-navigation/core';
import Images from '../assets/images';
import { TextStyles } from '../styles';
import { PRIMARY } from '../styles/Colors';

interface NavbarProps {
  title: string;
  onBack?: () => void;
  hasProfile?: boolean;
}

const FocusAwareStatusBar = (props: StatusBarProps) => {
  const isFocused = useIsFocused();

  return isFocused ? <StatusBar {...props} /> : null;
};

const Navbar: React.FC<NavbarProps> = ({title, onBack, hasProfile}) => {
  return (
    <SafeAreaView style={{backgroundColor: PRIMARY}}>
      <View style={[styles.container, styles.shadow]}>
        <FocusAwareStatusBar
          backgroundColor={PRIMARY}
          barStyle={"light-content"}
        />
        <TouchableOpacity
          disabled={!onBack}
          style={styles.iconContainer}
          onPress={onBack}>
          {onBack && <Image source={Images.icBack} style={styles.icon} />}
        </TouchableOpacity>
        <Text numberOfLines={1} style={{...TextStyles.Caption24Bold, color: '#FFFFFF', textAlign: 'center', flex: 1}}>
          {title}
        </Text>
      </View>
    </SafeAreaView>
  )
};

export default Navbar;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: PRIMARY,
    paddingHorizontal: 12,
    paddingVertical: 16,
    zIndex: 5,
  },
  iconContainer: {
    width: 32,
    height: 32,
    justifyContent: 'center',
  },
  icon: {
    width: 32,
    height: 32,
    tintColor: '#FFFFFF',
    resizeMode: 'contain'
  },
  shadow: {
    shadowColor: 
      Platform.OS === 'ios' ? 'rgba(0, 0, 0, 0.25)' : 'rgba(0, 0, 0, 0.5)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    elevation: 20,
    shadowRadius: 10,
    shadowOpacity: 0.5,
  }
});
