import React from 'react';
import {
  View, 
  Text, 
  Image,
  StyleSheet, 
  TouchableOpacity, 
  ActivityIndicator, 
  ViewStyle, 
  StyleProp, 
  TextStyle, 
  ImageStyle, 
  ImageSourcePropType
} from 'react-native';
import { BUTTON, BUTTON_INACTIVE, TEXT_TERITARY } from '../styles/Colors';

interface Props {
  title: string;
  style?: StyleProp<ViewStyle>,
  isLoading?: boolean;
  isTransparent?: boolean;
  buttonStyle?: StyleProp<ViewStyle>,
  disabled?: boolean;
  onPress: () => void;
  icon?: ImageSourcePropType
  color?: string;
}

const Button: React.FC<Props> = ({
  title,
  style,
  isLoading,
  isTransparent,
  buttonStyle,
  disabled,
  onPress,
  icon,
  color
}) => {
  return (
    <View style={[styles({}).container, style]}>
      <TouchableOpacity
        onPress={onPress}
        disabled={disabled || isLoading}
        style={[styles({disabled, isTransparent, color}).button, buttonStyle]}>
        {!isLoading && icon ? (
          <Image source={icon} style={styles({}).icon} />
        ) : null}
        {isLoading ? (
          <ActivityIndicator color="#FFFFFF"/> 
        ) : (
          <Text style={styles({ disabled, isTransparent, color }).title}>{title}</Text>
        )}
      </TouchableOpacity>
    </View>
  );
}

export default Button;

interface StylesProps {
  disabled?: boolean;
  isTransparent?: boolean;
  color?: string;
}

interface StyleSheetType {
  container: ViewStyle;
  button: ViewStyle;
  title: TextStyle;
  icon: ImageStyle;
}

type StylesFunctionProps = (props: StylesProps) => StyleSheetType;

const styles: StylesFunctionProps = ({ disabled, isTransparent, color }) =>
  StyleSheet.create<StyleSheetType>({  
    container: {
      padding: 0,
      paddingVertical: 16
    },
    button: {
      height: 48,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor: isTransparent ? 'transparent' 
        : disabled
        ? BUTTON_INACTIVE
        : BUTTON,
      alignItems: 'center',
      paddingVertical: 8,
      paddingHorizontal: 14,
      borderRadius: 32,
      borderWidth: isTransparent ? 1 : 0,
      borderColor: color || BUTTON
    },
    title: {
      color: disabled ? TEXT_TERITARY : isTransparent ? color || BUTTON : '#FFFFFF', 
      fontWeight: 'bold'
    },
    icon: {
      width: 24,
      height: 24,
      marginRight: 10,
      resizeMode: 'contain'
    }
});
