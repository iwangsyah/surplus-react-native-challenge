import React from 'react';
import { StyleSheet, Image, View, Text, Dimensions } from 'react-native';
import Modal from 'react-native-modal';
import { Button } from '.';
import images from '../assets/images';
import { TextStyles } from '../styles';
import { BUTTON } from '../styles/Colors';

const { width } = Dimensions.get('window')

interface ModalProps {
  visible: boolean;
  onClose: () => void;
}

const ModalDevelopment: React.FC<ModalProps> = ({
  visible, 
  onClose
}) => {

  return (
    <View>
      <Modal
        animationIn="slideInUp"
        isVisible={visible}
        style={{alignItems: 'center', margin: 20}}>
        <View style={styles.modalView}>
          <View style={{alignItems: 'center', paddingHorizontal: 16}}>
            <Image
              source={images.icDevelopment}
              style={{width: width / 3, height: width / 3, marginBottom: 24}}
              resizeMode="contain"
            />
            <Text style={TextStyles.Caption16Bold}>Layanan Segera Hadir</Text>
            <Text style={{...TextStyles.Caption14Regular, textAlign: 'center', marginTop: 16}}>Layanan ini sedang dalam proses pengembangan</Text>
          </View>
          <Button
            title='Tutup' 
            style={{padding: 24, marginVertical: 16}} 
            buttonStyle={{borderRadius: 4}}
            onPress={onClose}
          />
        </View>
      </Modal>
    </View>
  );
}

export default ModalDevelopment;

const styles = StyleSheet.create({
  modalView: {
    width: '90%',
    backgroundColor: '#FFFFFF',
    borderRadius: 12,
    paddingTop: 24,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    alignItems: 'center',
    backgroundColor: BUTTON,
    borderRadius: 8,
    marginTop: 16,
    padding: 8,
    marginHorizontal: 4
  },
});
