import React from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  TextInput,
  Pressable,
  ViewStyle,
  TextStyle,
  ImageStyle,
  TextInputProps,
} from 'react-native';
import Images from '../assets/images';
import { BUTTON_INACTIVE, LINE, TEXT_PRIMARY } from '../styles/Colors';
import { TextStyles } from '../styles';

interface ExtraInputProps {
  onPressRight?: () => void;
  isPassword?: boolean;
  hideIcon?: boolean;
  color?: string;
  title:string;
  errorText?: string;
}

type InputProps = TextInputProps & ExtraInputProps

const InputText: React.FC<InputProps> = (props) => {
  const {
    placeholderTextColor,
    secureTextEntry,
    autoCapitalize,
    keyboardType,
    onChangeText,
    onPressRight,
    placeholder,
    isPassword,
    multiline,
    errorText,
    editable,
    hideIcon,
    title,
    value,
    style,
    color
} = props

  return (
    <View style={[{marginTop: 16}, style]}>
      {title && <Text style={styles({color}).title}>{title}</Text>}
      <View style={[styles({color, editable}).container, multiline && {height: 100}]}>
        <TextInput
          editable={editable}
          multiline={multiline}
          keyboardType={keyboardType}
          placeholder={placeholder}
          placeholderTextColor={placeholderTextColor || '#AAAAAA'}
          secureTextEntry={secureTextEntry}
          onChangeText={onChangeText}
          style={[styles({}).input, multiline && {height: 100, textAlignVertical: 'top'}]}
          autoCapitalize={autoCapitalize}
          returnKeyType="done"
          value={value}
        />
        {isPassword && !hideIcon && (
          <Pressable onPress={onPressRight} style={styles({}).iconContainer}>
            <Image
              style={styles({}).icon}
              source={secureTextEntry ? Images.icEyeOpen : Images.icEyeClose}
            />
          </Pressable>
        )}
      </View>
      {errorText && <Text style={styles({}).errorText}>{errorText}</Text>}
    </View>
  );
}

export default InputText;

interface StylesProps {
  color?: string;
  editable?: boolean;
}

interface StyleSheetType {
  container: ViewStyle;
  title: TextStyle;
  input: TextStyle;
  iconContainer: ViewStyle;
  icon: ImageStyle;
  errorText: TextStyle;
}

type StylesFunctionProps = (props: StylesProps) => StyleSheetType;

const styles: StylesFunctionProps = ({ color, editable }) =>
  StyleSheet.create<StyleSheetType>({    
    container: {
      height: 48,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: editable === false ? '#D8D8D8' : '#FFFFFF',
      justifyContent: 'space-between',
      borderRadius: 6,
      paddingLeft: 16,
      paddingRight: 0,
      borderWidth: 1,
      borderColor: color || LINE,
    },
    title: {
      color: color || TEXT_PRIMARY,
      fontWeight: 'bold', 
      marginBottom: 8, 
    },
    input: {
      flex: 1,
      height: '100%',
      color: TEXT_PRIMARY,
    },
    iconContainer: {
      padding: 10,
      paddingRight: 16,
    },
    icon: {
      width: 24,
      height: 24,
      marginRight: 0,
      tintColor: BUTTON_INACTIVE,
      resizeMode: 'contain'
    },
    errorText: {
      ...TextStyles.Caption12Regular,
      marginTop: 4,
      width: '100%',
      color: 'red',
    },
});