import React, {useEffect, useState} from 'react';
import { Image, Dimensions, SafeAreaView} from 'react-native';
import { NavigationContainer, useNavigationContainerRef } from '@react-navigation/native';
import { createStackNavigator, StackScreenProps } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { PRIMARY, TEXT_TERITARY } from '../styles/Colors';
import { BottomTabBarStyle } from '../styles';
import { useSelector } from 'react-redux';
import Images from '../assets/images';
import * as Screen from '../screens';

export type TabStackParamList = {
  HomeTabs: undefined;
  ProfileTabs: undefined
}

export type AuthStackParamList = {
  Login: undefined;
  Register: undefined;
}

export type HomeStackParamList = {
  Home: undefined;
  Detail?: any;
}

export type ProfileStackParamList = {
  Profile: undefined;
}

const {width, height} = Dimensions.get('window');

export type AuthScreenProps<RouteName extends keyof AuthStackParamList> = StackScreenProps<AuthStackParamList, RouteName>;
export type HomeScreenProps<RouteName extends keyof HomeStackParamList> = StackScreenProps<HomeStackParamList, RouteName>;
export type ProfileScreenProps<RouteName extends keyof ProfileStackParamList> = StackScreenProps<ProfileStackParamList, RouteName>;

const AuthStack = createStackNavigator<AuthStackParamList>();
const HomeStack = createStackNavigator<HomeStackParamList>();
const MessageStack = createStackNavigator<ProfileStackParamList>();
const Tab = createBottomTabNavigator<TabStackParamList>();

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator screenOptions={{headerShown: false}}>
      <HomeStack.Screen name='Home' component={Screen.Home} />
      <HomeStack.Screen name='Detail' component={Screen.Detail} />
    </HomeStack.Navigator>
  );
};

const ProfileStackScreen = () => {
  return (
    <MessageStack.Navigator screenOptions={{headerShown: false}}>
      <MessageStack.Screen name='Profile' component={Screen.Profile} />
    </MessageStack.Navigator>
  );
};

const AuthStackScreen = () => {
  return (
    <AuthStack.Navigator screenOptions={{headerShown: false}}>
      <AuthStack.Screen name='Login' component={Screen.Login} />
      <AuthStack.Screen name='Register' component={Screen.Register} />
    </AuthStack.Navigator>
  );
};

const TabBarStackScreen = () => (
  <Tab.Navigator
    initialRouteName='HomeTabs'
    screenOptions={({route}) => ({
      tabBarStyle: BottomTabBarStyle.container,
      tabBarLabelStyle: BottomTabBarStyle.label,
      tabBarIcon: ({focused, size}) => {
        let icon;
        if (route.name === 'HomeTabs') {
          icon = Images.icHome;
        } else if (route.name === 'ProfileTabs') {
          icon = Images.icPerson;
        }
        return (
          <Image
            source={icon}
            style={{
              width: size,
              height: size,
              resizeMode: 'contain',
              tintColor: focused ? PRIMARY : TEXT_TERITARY
            }}
          />
        );
      },
      tabBarActiveTintColor: PRIMARY,
      tabBarInactiveTintColor: TEXT_TERITARY,
      tabBarVisible: false,
    })}>
    <Tab.Screen
      name="HomeTabs"
      component={HomeStackScreen}
      options={{
        headerShown: false,
        tabBarLabel: 'Home',
      }}
    />
    <Tab.Screen
      name="ProfileTabs"
      component={ProfileStackScreen}
      options={{
        headerShown: false,
        tabBarLabel: 'Profile',
      }}
    />
  </Tab.Navigator>
);

const AppContainer = () => {
  const [isLoading, setIsLoading] = useState(true);
  const userToken = useSelector((state: any) => state.user.token);
  const navigationRef = useNavigationContainerRef();

  useEffect(() => {
    setTimeout(() => getUserToken(), 2000);
  },[]);

  const getUserToken = async () => {
    setIsLoading(false);
  };

  if (isLoading) {
    return (
      <SafeAreaView style={{flex: 1}}>
        <Image source={Images.bgSplash} style={{width, height, resizeMode:'contain'}} />
      </SafeAreaView>
    );
  }

  return (
    <NavigationContainer ref={navigationRef}>
      {userToken ? (
        <TabBarStackScreen />
      ) : (
        <AuthStackScreen />
      )}
    </NavigationContainer>
  );
};
export default AppContainer;
