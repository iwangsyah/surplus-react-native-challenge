import AsyncStorage from '@react-native-async-storage/async-storage';
import {configureStore} from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import rootReducer from '../reducers';

type RootReducer = ReturnType<typeof rootReducer>;

const persistConfig = {
  key: 'root2',
  storage: AsyncStorage,
  whitelist: ['user']
};
const finalReducer = persistReducer<RootReducer>(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: finalReducer,
  middleware: [sagaMiddleware, logger],
});

const persistor = persistStore(store);

export default {
  store,
  persistor
}

