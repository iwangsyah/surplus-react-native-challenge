import {ActionTypes as types} from '../configs';
import { Alert } from "react-native";
import Store from './Store';

const ErrorHanlder = async (error : any) => {
  let er = error;
  let status = 0;
  let statusText = 'Please try again';

  if (er?.response) {
    const errorJson = JSON.stringify(er.response);
    console.log(er?.response);

    status = er?.response.status;
    let message = er?.response?.data?.message;
    
    if (message === 'Unauthenticated.') {
      Alert.alert('Login session expired\nPlease login again.'),
      Store.store.dispatch({ type: types.SET_TOKEN, payload: null })
    }

    if ([500, 409, 401, 400, 422].includes(status)) {
      statusText = er?.response.data?.error?.message || er.response.data?.error?.code || 'Failed';
    }
  } else if (er.request) {
    const errorJson = JSON.stringify(er.request);
    console.log(`error log request ${errorJson}`);

    status = er.request.status;

    if (er.request.status === 0) {
      statusText = 'Please check your connection';
    }
  } else {
    console.log(`error log default ${er}`);
  }
  er = {
    status,
    statusText
  };

  return er;
};

export default ErrorHanlder;
