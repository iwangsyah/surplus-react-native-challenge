import {Dimensions, Platform, StatusBar, ViewStyle} from 'react-native';

const isIphoneX = () => {
  const dimen = Dimensions.get('window');
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    (dimen.height === 812 ||
      dimen.width === 812 ||
      dimen.width >= 844 ||
      dimen.height >= 844 ||
      dimen.height >= 896 ||
      dimen.width >= 896)
  );
};

const ifIphoneX = (iphoneXStyle : number, regularStyle : number) => {
  if (isIphoneX()) {
    return iphoneXStyle;
  }
  return regularStyle;
};

const getStatusBarHeight = (safe : boolean) =>
  Platform.select({
    ios: ifIphoneX(safe ? 44 : 44, 20),
    android: StatusBar.currentHeight,
  });

const getBottomSpace = () => (isIphoneX() ? 34 : 0);

export default {
  isIphoneX,
  ifIphoneX,
  getStatusBarHeight,
  getBottomSpace,
};
