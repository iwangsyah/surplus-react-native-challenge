import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  FlatList,
  Text,
  View,
  Image,
  Alert,
  TextInput,
  ListRenderItem,
  SafeAreaView,
  RefreshControl,
  Pressable,
} from 'react-native';
import _ from 'lodash';
import Images from '../../assets/images';
import TextStyles from '../../styles/TextStyles';
import { useSelector } from 'react-redux';
import { ApiService } from '../../services';
import { BG_PRIMARY, LINE, PRIMARY, TEXT_PLACEHOLDER, TEXT_SECONDARY, TEXT_TERITARY } from '../../styles/Colors';
import { ErrorHandler } from '../../util';
import { HomeScreenProps } from '../../containers/Router';

const Home = ({navigation}: HomeScreenProps<'Home'>) => {
  const [data, setData] = useState([])
  const [dataShow, setDataShow] = useState([])
  const [keyword, setKeyword] = useState('')
  const token = useSelector((state : any) => state.user.token);

  useEffect(() => {
    getData()
  }, [])

  const getData = () => {
    ApiService.getData()
      .then((respone) => {
        const { data } = respone;
        setData(data)
        setDataShow(data)
        setKeyword('')
      })
     .catch((error) => {
        ErrorHandler(error).then((er) => {
          Alert.alert(er.statusText)
        })
      })
  }

  const onSearch = (text: string) => {
    setKeyword(text)
    text = text.toLowerCase().replace(/\s/g, '')
    const dataFilter = data.filter((item: any) => {
      const name = item.name.toLowerCase().replace(/\s/g, '')
      return name.includes(text)
    })
    setDataShow(dataFilter)
  }

  const renderItem: ListRenderItem<any> = ({item}) => {
    return (
      <Pressable style={styles.item} onPress={() => navigation.navigate('Detail', {...item})}>
        <Text style={TextStyles.Caption14SemiBold}>{item?.name}</Text>
        <Image source={Images.icChevronRight} style={styles.icRight} />
      </Pressable>
    )
  }


  return (
    <SafeAreaView style={{flex: 1, backgroundColor: BG_PRIMARY}}>
      <View style={styles.header}>
        <Text style={{...TextStyles.Caption16Bold, color: TEXT_SECONDARY}}>Your Location here</Text>
        <View>
          <Text style={{...TextStyles.Caption24Bold, color: TEXT_SECONDARY}}>Hi, {token}</Text>
          <View style={styles.search}>
            <TextInput
              placeholder='Cari...'
              placeholderTextColor={TEXT_PLACEHOLDER}
              onChangeText={onSearch}
              style={{flex: 1}}
              returnKeyType="done"
              value={keyword}
            />
            <Image
              style={{width: 24, height: 24}}
              source={Images.icSearch}
            />
          </View>
        </View>
      </View>
      <View style={{flex: 1, paddingTop: 16}}>
        <View style={styles.title}>
          <Text style={TextStyles.Caption20Bold}>Brewery List</Text>
        </View>
        <View style={{flex: 1}}>
          <FlatList
            data={dataShow}
            renderItem={renderItem}
            keyExtractor={(_, index) => String(index)}
            contentContainerStyle={styles.flatlist}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
            ListEmptyComponent={() => <Text style={styles.empty}>Data Kosong</Text>}
            refreshControl={
              <RefreshControl
                refreshing={false}
                onRefresh={() => getData()}
              />
            }
          /> 
        </View>     
      </View>
    </SafeAreaView>
  );
}

export default Home;

const styles = StyleSheet.create({
  header: {
    width: '110%', 
    height: '30%', 
    alignSelf: 'center',
    justifyContent: 'space-between',
    backgroundColor: PRIMARY,
    borderBottomLeftRadius: 120,
    borderBottomRightRadius: 120,
    paddingHorizontal: 40,
    paddingVertical: 24,
    paddingBottom: 0
  },
  search: {
    height: 45, 
    flexDirection: 'row', 
    alignItems: 'center',
    backgroundColor: BG_PRIMARY, 
    borderColor: LINE,
    borderRadius: 8,
    borderWidth: 1, 
    paddingHorizontal: 16,
    marginTop: 16
  },
  title: {
    padding: 16,
    borderColor: LINE,
    borderBottomWidth: 1,
    marginBottom: 8,
  },
  flatlist: {
    flexGrow: 1, 
    paddingHorizontal: 16, 
    paddingBottom: 16
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
  },
  icRight: {
    width: 21,
    height: 21,
    resizeMode: 'contain'
  },
  separator: {
    height: 1,
    backgroundColor: LINE,
    marginVertical: 8,
  },
  empty: {
    textAlign: 'center', 
    marginTop: 32, 
    color: TEXT_TERITARY
  }
});
