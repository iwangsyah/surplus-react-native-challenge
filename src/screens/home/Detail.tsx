import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  ScrollView,
} from 'react-native';
import _ from 'lodash';
import { BG_PRIMARY } from '../../styles/Colors';
import { Navbar } from '../../components';
import { HomeScreenProps } from '../../containers/Router';
import { TextStyles } from '../../styles';

const Detail = ({navigation, route}: HomeScreenProps<'Detail'>) => {
  const data = route.params

  const renderItem = () => {
    let item: JSX.Element[] = [];
    Object.entries(data || {}).forEach(([key, value]) => {
      item.push(
        <View style={styles.itemContainer}>
          <Text style={styles.title}>{_.startCase(_.toLower( key.replace('_', ' ')))}</Text>
          <Text style={{flex: 1}}>:</Text>
          <Text style={styles.value}>{value || '-'}</Text>
        </View>
      )
    })
    return item;
  }
  
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: BG_PRIMARY}}>
      <Navbar 
        title={data?.name} 
        onBack={() => navigation.goBack()}
      />
      <ScrollView contentContainerStyle={{flex: 1, padding: 16}}>
        {renderItem()}
      </ScrollView>
    </SafeAreaView>
  );
}
export default Detail;

const styles = StyleSheet.create({
  itemContainer: {
    flex: 1, 
    flexDirection: 'row',
    marginBottom: 8
  },
  title: {
    ...TextStyles.Caption14Bold, 
    flex: 3
  },
  value: {
    ...TextStyles.Caption14Regular, 
    flexWrap: 'wrap',
    flex: 5.5
  }
});
