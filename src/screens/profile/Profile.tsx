import React, {useEffect, useState} from 'react';
import { 
  Image, 
  View, 
  Text, 
  StyleSheet, 
  TouchableOpacity, 
  ScrollView, 
  Alert
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import { BG_PRIMARY, LINE, PRIMARY, TEXT_SECONDARY } from '../../styles/Colors';
import { ProfileScreenProps } from '../../containers/Router';
import { Button, ModalDevelopment } from '../../components';
import { ActionTypes as types } from '../../configs';
import { TextStyles } from '../../styles';
import Images from '../../assets/images';

const KEYCHANGEPASSWORD = 'KEYCHANGEPASSWORD';
const KEYHELP = 'KEYHELP';
const KEYTERMSSERVICE = 'KEYTERMSSERVICE';

const Profile = ({navigation}: ProfileScreenProps<'Profile'>) => {
  const [visible, setVisible] = useState(false);
  const token = useSelector((state : any) => state.user.token);
  const dispatch = useDispatch();

  useEffect(() => {
  }, []);


  const renderSetting = () => {
    const listGeneral = [
      {
        key: KEYCHANGEPASSWORD,
        title: 'Change Password',
      },
    ];

    return listGeneral.map((item, index) => (
      <TouchableOpacity
        key={`${item}+${index}`}
        style={styles.item}
        onPress={() => setVisible(true)}>
        <Text>{item.title}</Text>
        <Image source={Images.icChevronRight} style={styles.icRight} />
      </TouchableOpacity>
    ));
  };

  const renderLegal = () => {
    const listGeneral = [
      {
        key: KEYHELP,
        title: 'Help',
      },
      {
        key: KEYTERMSSERVICE,
        title: 'Terms of Service',
      },
    ];

    return listGeneral.map((item, index) => (
      <TouchableOpacity
        key={`${item}+${index}`}
        style={styles.item}       
        onPress={() => setVisible(true)}>
        <Text>{item.title}</Text>
        <Image source={Images.icChevronRight} style={styles.icRight} />
      </TouchableOpacity>
    ));
  };

  const onLogout = () => {
    Alert.alert('Confirmation', 'Are you sure you want to logout?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => dispatch({type: types.SET_TOKEN, payload: null}),
      },
    ]);
  }

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.header}>
          <Image source={Images.icProfile} style={styles.icProfile} />
          <Text style={{...TextStyles.Caption20Bold, color: TEXT_SECONDARY, marginTop: 16}}>{token}</Text>
        </View>
        <View style={{flex: 1, padding: 16}}>
          <Text style={{...TextStyles.Caption16Bold, marginVertical: 8}}>Settings</Text>
          {renderSetting()}
          <View style={styles.separator} />
          <Text style={{...TextStyles.Caption16Bold, marginVertical: 8}}>Legal and Support</Text>
          {renderLegal()}
          <View style={styles.separator} />
        </View>
        <Button
          isTransparent
          title='Keluar' 
          onPress={onLogout}
          style={{padding: 16}}
          color='red'
        />
      </ScrollView>
      <ModalDevelopment 
        visible={visible} 
        onClose={() => setVisible(false)} 
      />
    </View>
  );
}

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BG_PRIMARY,
  },
  header: {
    height: '30%', 
    backgroundColor: PRIMARY, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
  },
  icProfile: {
    width: 100, 
    height: 100, 
    resizeMode: 'contain'
  },
  icRight: {
    width: 21,
    height: 21,
    resizeMode: 'contain'
  },
  separator: {
    height: 1,
    backgroundColor: LINE,
    marginVertical: 8,
  },
});
