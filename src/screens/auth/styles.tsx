import { Dimensions, StyleSheet } from "react-native";
import { TextStyles } from "../../styles";
import { BG_PRIMARY, BUTTON_INACTIVE, PRIMARY, TEXT_PLACEHOLDER, TEXT_TERITARY } from "../../styles/Colors";

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BG_PRIMARY
  },
  imageHeader: {
    width, 
    height: width * 55 / 100, 
    resizeMode: 'stretch',
    justifyContent: 'flex-end',
    paddingHorizontal: 24,
    paddingBottom: 45
  },
  content: {
    flex: 1,
    backgroundColor: BG_PRIMARY,
    paddingHorizontal: 24,
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    paddingTop: 32,
    bottom: 30
  },
  forgotPassword: {
    textAlign: 'right',
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 16,
    width: '100%',
    color: TEXT_TERITARY
  },
  atau: {
    textAlign: 'center',
    marginVertical: 16,
    marginHorizontal: 16,
    color: TEXT_PLACEHOLDER
  },
  button: {
    flex: 1,
    height: 48,
    flexDirection: 'row',
    backgroundColor: BUTTON_INACTIVE,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 32
  },
  icon: {
    width: 16, 
    height: 16, 
    marginRight: 10, 
    resizeMode: 'contain'
  },
  privacy: {
    ...TextStyles.Caption13Medium, 
    color: TEXT_TERITARY, 
    textAlign: 'center'
  },
  registerLogin: {
    ...TextStyles.Caption14SemiBold,
    textDecorationLine: 'underline',
    color: PRIMARY
  }
});

export default styles;
