import React, { useState } from 'react';
import { View, Text, Image, ImageBackground, SafeAreaView, ScrollView, Pressable } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Button, InputText, ModalSuccess, ModalDevelopment } from '../../components';
import { AuthScreenProps } from '../../containers/Router';
import { ActionTypes as types } from '../../configs';
import { TEXT_SECONDARY } from '../../styles/Colors';
import { FormValidator } from '../../util';
import { TextStyles } from '../../styles';
import Images from '../../assets/images'
import styles from './styles';

const Register = ({navigation}: AuthScreenProps<'Register'>) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [passwordRepeat, setPasswordRepeat] = useState('')
  const [isLoadingLogin, setIsLoadingLogin] = useState(false)
  const [secureTextEntry, setSecureTextEntry] = useState(true)
  const [secureTextEntryRepeat, setSecureTextEntryRepeat] = useState(true)
  const [errorTextEmail, setErrorTextEmail] = useState('')
  const [errorTextPassword, setErrorTextPassword] = useState('')
  const [errorTextPasswordRepeat, setErrorTextPasswordRepeat] = useState('')
  const [visibleSuccess, setVisibleSuccess] = useState(false)
  const [visible, setVisible] = useState(false);
  const users = useSelector((state : any) => state.user.users) || [];
  const dispatch = useDispatch();

  const onLogin = () => {
    const registered = users.find((user: {email: string, password: string}) => user.email === email)
    if (registered) {
      setErrorTextEmail('Email sudah terdaftar')
      setTimeout(() => {
        setErrorTextEmail('')
      }, 3000)
      return false
    }
    if (!FormValidator.email(email)) {
      setErrorTextEmail('Alamat email tidak sesuai')
      setTimeout(() => {
        setErrorTextEmail('')
      }, 3000)
      return false
    }
    if (password.length < 6) {
      setErrorTextPassword('Password minimal 6 karakter')
      setTimeout(() => {
        setErrorTextPassword('')
      }, 3000)
      return false
    }
    if (password !== passwordRepeat) {
      setErrorTextPasswordRepeat('Password tidak sama')
      setTimeout(() => {
        setErrorTextPasswordRepeat('')
      }, 3000)
      return false
    }

    setIsLoadingLogin(true)
    const newUsers = [...users, {email, password}];
    
    dispatch({ type: types.SET_USERS, payload: newUsers })
    setIsLoadingLogin(false)

    setVisibleSuccess(true)
    setTimeout(() => {
      setVisibleSuccess(false)
      navigation.navigate('Login')
    }, 3000)
  }

  const goToLogin = () => {
    navigation.navigate('Login')
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <ImageBackground source={Images.bgHeaderLogin} style={styles.imageHeader}>
          <Text style={{...TextStyles.Caption24Bold, color: TEXT_SECONDARY, marginBottom: 8}}>Daftar</Text>
          <Text style={{...TextStyles.Caption14Regular, color: TEXT_SECONDARY}}>{'Lengkapi isian untuk mendaftar'}</Text>
        </ImageBackground>
        <View style={styles.content}>
          <InputText 
            title='E-mail' 
            placeholder='Alamat email kamu' 
            onChangeText={setEmail} 
            keyboardType='email-address'
            autoCapitalize='none'
            value={email} 
            errorText={errorTextEmail}
          />
          <InputText
            isPassword 
            title='Kata sandi' 
            placeholder='Masukkan kata sandi' 
            onPressRight={() => setSecureTextEntry(!secureTextEntry)}
            secureTextEntry={secureTextEntry}
            onChangeText={setPassword} 
            autoCapitalize='none'
            value={password} 
            errorText={errorTextPassword}
          />
          <InputText
            isPassword 
            title='Ulang kata sandi' 
            placeholder='Ulangi kata sandi' 
            onPressRight={() => setSecureTextEntryRepeat(!secureTextEntryRepeat)}
            secureTextEntry={secureTextEntryRepeat}
            onChangeText={setPasswordRepeat} 
            autoCapitalize='none'
            value={passwordRepeat} 
            errorText={errorTextPasswordRepeat}
          />
          <Button
            title='Daftar' 
            disabled={!email || !password || !passwordRepeat}
            isLoading={isLoadingLogin}
            onPress={onLogin}
            style={{marginTop: 16}}
          />  
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 1, height: 1, backgroundColor: '#E5E5E5'}} />
            <Text style={styles.atau}>Atau</Text>  
            <View style={{flex: 1, height: 1, backgroundColor: '#E5E5E5'}} />
          </View>  
          <View style={{flexDirection: 'row', marginBottom: 32}}>
            <Pressable style={styles.button} onPress={() => setVisible(true)}>
              <Image source={Images.icFacebook} style={styles.icon} />
              <Text>Facebook</Text>
            </Pressable>
            <View style={{width: 16}} />
            <Pressable style={styles.button} onPress={() => setVisible(true)}>
              <Image source={Images.icGoogle} style={styles.icon} />
              <Text>Google</Text>
            </Pressable>
          </View>
          <Text style={styles.privacy}>
            Dengan mendaftar, Anda menerima
            <Text style={{...styles.privacy, color: 'orange', textDecorationLine: 'underline'}}> syarat dan ketentuan</Text>
            <Text style={styles.privacy}> serta</Text>
            <Text style={{...styles.privacy, color: 'orange', textDecorationLine: 'underline'}}> kebijakan privasi</Text>
          </Text>

          <Text style={{textAlign: 'center', marginTop: 32}}>
            Sudah punya akun?
            <Text style={styles.registerLogin} onPress={goToLogin}> Yuk masuk</Text>
          </Text>
        </View>
      </ScrollView>
      <ModalSuccess 
        visible={visibleSuccess} 
        title='Akun berhasil didaftarkan' 
      />
      <ModalDevelopment
        visible={visible} 
        onClose={() => setVisible(false)} 
      />
    </SafeAreaView>
  );
}

export default Register;