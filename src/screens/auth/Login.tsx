import React, { useState } from 'react';
import { View, Text, Image, ImageBackground, SafeAreaView, ScrollView, Pressable } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Button, InputText, ModalDevelopment } from '../../components';
import { AuthScreenProps } from '../../containers/Router';
import { ActionTypes as types } from '../../configs';
import { TEXT_SECONDARY } from '../../styles/Colors';
import { TextStyles } from '../../styles';
import Images from '../../assets/images';
import styles from './styles';

const Login = ({navigation}: AuthScreenProps<'Login'>) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isLoadingLogin, setIsLoadingLogin] = useState(false)
  const [secureTextEntry, setSecureTextEntry] = useState(true)
  const [errorTextEmail, setErrorTextEmail] = useState('')
  const [errorTextPassword, setErrorTextPasswordl] = useState('')
  const [visible, setVisible] = useState(false);
  const users = useSelector((state : any) => state.user.users) || [];
  const dispatch = useDispatch();

  const onLogin = () => {
    const registered = users.find((user: {email: string, password: string}) => user.email === email)
    if (registered) {
      if (registered.password === password) {
        setIsLoadingLogin(true)
        dispatch({ type: types.SET_TOKEN, payload: email })
        setIsLoadingLogin(false)
      } else {
        setErrorTextPasswordl('Password salah')
        setTimeout(() => {
          setErrorTextPasswordl('')
        }, 3000)
      }
    } else {
      setErrorTextEmail('Email belum terdaftar')
      setTimeout(() => {
        setErrorTextEmail('')
      }, 3000)
    }
  }

  const goToRegister = () => {
    navigation.navigate('Register')
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <ImageBackground source={Images.bgHeaderLogin} style={styles.imageHeader}>
          <Text style={{...TextStyles.Caption24Bold, color: TEXT_SECONDARY, marginBottom: 8}}>Masuk</Text>
          <Text style={{...TextStyles.Caption14Regular, color: TEXT_SECONDARY}}>{'Pastikan kamu sudah pernah\nmembuat akun Surplus'}</Text>
        </ImageBackground>
        <View style={styles.content}>
          <InputText 
            title='E-mail' 
            placeholder='Alamat email kamu' 
            onChangeText={setEmail} 
            keyboardType='email-address'
            autoCapitalize='none'
            value={email} 
            errorText={errorTextEmail}
          />
          <InputText
            isPassword 
            title='Kata sandi' 
            placeholder='Masukkan kata sandi' 
            onPressRight={() => setSecureTextEntry(!secureTextEntry)}
            secureTextEntry={secureTextEntry}
            onChangeText={setPassword} 
            autoCapitalize='none'
            value={password} 
            errorText={errorTextPassword}
          />
          <Text style={styles.forgotPassword}>Lupa kata sandi?</Text>
          <Button
            title='Masuk' 
            disabled={!email || !password}
            isLoading={isLoadingLogin}
            onPress={onLogin}
            style={{marginTop: 16}}
          />    
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 1, height: 1, backgroundColor: '#E5E5E5'}} />
            <Text style={styles.atau}>Atau</Text>  
            <View style={{flex: 1, height: 1, backgroundColor: '#E5E5E5'}} />
          </View>            
          <View style={{flexDirection: 'row', marginBottom: 32}}>
            <Pressable style={styles.button} onPress={() => setVisible(true)}>
              <Image source={Images.icFacebook} style={styles.icon} />
              <Text>Facebook</Text>
            </Pressable>
            <View style={{width: 16}} />
            <Pressable style={styles.button} onPress={() => setVisible(true)}>
              <Image source={Images.icGoogle} style={styles.icon} />
              <Text>Google</Text>
            </Pressable>
          </View>
          <Text style={{textAlign: 'center'}}>
            Belum punya akun?
            <Text style={styles.registerLogin} onPress={goToRegister}> Yuk daftar</Text>
          </Text>
        </View>
      </ScrollView>
      <ModalDevelopment
        visible={visible} 
        onClose={() => setVisible(false)} 
      />
    </SafeAreaView>
  );
}

export default Login;