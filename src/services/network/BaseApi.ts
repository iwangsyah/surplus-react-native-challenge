import Axios from 'axios';
import _ from 'lodash';
import { Api } from '../../configs';
import { Store } from '../../util';

type User = {
  token?: any
}

const interceptorsRequest = (instance ?: any) =>
  instance.interceptors.request.use(
    async (config : any) => {
      const user: User = Store.store.getState().user
      const header = {
        Accept: 'application/json',
        'content-type': 'application/json',
        Authorization: user?.token
      };

      config.headers = {
        ...config.headers,
        ...header,
      };

      return config;
    },
    // Do something before request is sent
    (error : any) => Promise.reject(error),
  );

const interceptorsResponse = (instance ?: any) =>
  instance.interceptors.response.use(
    (response : any) => response,
    (error : any) => {
      if (_.isEmpty(error.response)) {
        return Promise.reject(error);
      }
      return Promise.reject(error);
    },
  );

const  BaseApi = () => {
  const source = Axios.CancelToken.source();
  setTimeout(() => {
    source.cancel();
  }, 10000);

  const instance = Axios.create({
    baseURL: Api.BASE_URL,
    timeout: 10000,
  });

  interceptorsRequest(instance);
  interceptorsResponse(instance);

  return instance;
};

export default BaseApi;
