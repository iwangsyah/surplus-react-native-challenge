import React from 'react';
import { LogBox } from 'react-native';
import _ from 'lodash';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Store } from './util';
import AppContainer from './containers/Router';

LogBox.ignoreAllLogs(true);

export default function App() {
  return (
    <Provider store={Store.store}>
      <PersistGate loading={null} persistor={Store.persistor}>
        <AppContainer />
      </PersistGate>
    </Provider>
  );
}
