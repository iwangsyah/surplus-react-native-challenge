export const PRIMARY = '#0F8B32';
export const BG_PRIMARY = '#FFFFFF';
export const BG_SECONDARY = '#F5F6F8';
export const BG_TERITARY = '#F5F6F8';
export const TEXT_PRIMARY = '#000000';
export const TEXT_SECONDARY = '#FFFFFF';
export const TEXT_TERITARY = '#B5B5B5';
export const TEXT_PLACEHOLDER = '#D6D6D6';
export const PRIMARY_HEADER = '#006D29';
export const BUTTON_INACTIVE = '#E5E5E5';
export const BUTTON_SECONDARY = '#1C58F2';
export const BUTTON = '#1DAB45';
export const LINE = '#C5C5C5'