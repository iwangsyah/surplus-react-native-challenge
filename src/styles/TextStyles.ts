import { StyleSheet } from 'react-native';
import { PRIMARY, TEXT_PRIMARY } from './Colors';

const TextStyles = StyleSheet.create({
  Subtitle1: {
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 21,
    color: TEXT_PRIMARY,
  },  
  Caption10SemiBold: {
    fontWeight: '700',
    fontSize: 10,
    color: TEXT_PRIMARY,
    textAlign: 'center'
  },  
  Caption14SemiBold: {
    fontWeight: '700',
    fontSize: 14,
    color: TEXT_PRIMARY,
  },  
  Caption16Bold: {
    fontWeight: 'bold',
    fontSize: 16,
    color: TEXT_PRIMARY,
  }, 
  Caption16Medium: {
    fontWeight: '500',
    fontSize: 16,
    color: TEXT_PRIMARY,
  },  
  Caption14Bold: {
    fontWeight: 'bold',
    fontSize: 14,
    color: TEXT_PRIMARY,
  },  
  Caption12Regular: {
    fontSize: 12,
    color: TEXT_PRIMARY,
  }, 
  Caption14Regular: {
    fontSize: 14,
    color: TEXT_PRIMARY,
  }, 
  Caption13Medium: {
    fontWeight: '500',
    fontSize: 13,
    color: TEXT_PRIMARY,
  }, 
  Caption20Bold: {
    fontWeight: 'bold',
    fontSize: 20,
    color: TEXT_PRIMARY,
  },  
  Caption24Bold: {
    fontWeight: 'bold',
    fontSize: 24,
    color: TEXT_PRIMARY,
  },  
  Caption24SemiBold: {
    fontWeight: '700',
    fontSize: 24,
    color: TEXT_PRIMARY,
  },  
})

export default TextStyles
