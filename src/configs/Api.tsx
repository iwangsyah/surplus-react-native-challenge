const Api = {
  BASE_URL: 'https://api.openbrewerydb.org/breweries',
};

export default Api;