const StorageKey = {
  AUTHTOKEN: 'authToken',
  TOKEN: 'token',
};

export default StorageKey;
