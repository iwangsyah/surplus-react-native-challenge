const ActionTypes = {
    SET_TOKEN: 'SET_TOKEN',
    SET_PROFILE: 'SET_PROFILE',
    SET_USERS: 'SET_USERS',
};

export default ActionTypes;
