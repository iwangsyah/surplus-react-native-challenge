const Navigation = {
  APP: 'App',
  AUTH: 'Auth',
  LOGIN: 'Login',
  ONBOARDING: 'Onboarding',

  TABS: 'Tabs',

  PROFILE: 'Profile',

};

export default Navigation;
