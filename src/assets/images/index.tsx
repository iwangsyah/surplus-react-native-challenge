const Images = {
  icBack: require('./icons/ic_back.png'),
  icChevronDown: require('./icons/ic_chevron_down.png'),
  icChevronRight: require('./icons/ic_chevron_right.png'),
  icChevronUp: require('./icons/ic_chevron_up.png'),
  icClose: require('./icons/ic_close.png'),
  icDevelopment: require('./icons/ic_development.png'),
  icEyeClose: require('./icons/ic_eye_close.png'),
  icEyeOpen: require('./icons/ic_eye_open.png'),
  icGoogle: require('./icons/ic_google.png'),
  icFacebook: require('./icons/ic_facebook.png'),
  icHome: require('./icons/ic_home.png'),
  icLogo: require('./icons/ic_logo.png'),
  icPerson: require('./icons/ic_person.png'),
  icProfile: require('./icons/ic_profile.png'),
  icSearch: require('./icons/ic_search.png'),
  icSuccess: require('./icons/ic_success.png'),
  bgSplash: require('./background/bg_splash.png'),
  bgHeaderLogin: require('./background/bg_header_login.png'),
  bgOnboarding: require('./background/bg_onboarding.png'),
};

export default Images;
