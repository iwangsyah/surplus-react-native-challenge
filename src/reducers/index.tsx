import {combineReducers} from '@reduxjs/toolkit';
import user from './User';

const appReducer = combineReducers({
  user,
});

const rootReducer = (state: any, action: any) =>
  appReducer(state, action);

export default rootReducer;
