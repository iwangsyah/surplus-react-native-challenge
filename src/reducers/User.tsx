import {ActionTypes as types} from '../configs';

const userReducer = (state = {}, action: any) => {
  switch (action.type) {
    case types.SET_TOKEN:
      return {...state, token: action.payload};
    case types.SET_PROFILE:
      return {...state, profile: action.payload};
    case types.SET_USERS:
      return {...state, users: action.payload};
    default:
      return state;
  }
};

export default userReducer;
